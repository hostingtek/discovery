-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Feb 25, 2013 alle 17:39
-- Versione del server: 5.5.29
-- Versione PHP: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `roundcubemail`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `discovery_cache`
--

CREATE TABLE IF NOT EXISTS `discovery_cache` (
  `username` varchar(128) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `imap_host` varchar(255) DEFAULT NULL,
  `smtp_host` varchar(255) DEFAULT NULL,
  `pass` varchar(128) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
