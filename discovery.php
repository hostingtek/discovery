<?php
/**
 *
 * Automatically find out the imap and smtp server from the users email-address
 * and set it as host
 *
 * Works with a whitelist that can be defined in the config-file
 * for this plugin: config.inc.php
 *
 * @version 0.1
 * @copyright Copyright (c) 2013-2018 Zironda Srl
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License
 * @author Lorenzo Valori <support@zironda.com>
 */
 
class discovery extends rcube_plugin
{
	private $_whitelist = array();
	public $task = "login|logout|mail";
	private $server = null;
	private $cached = false;
	
	function init()
	{
		//parse whitelist from config-file and turn "pseudo-regex" with *.domain.com to a regex
		if ($this->load_config())
		{
			$ar_whitelist = rcmail::get_instance()->config->get('discovery_whitelist');
			
			
			if (is_array($ar_whitelist) && count($ar_whitelist) > 0)
			{
				foreach ($ar_whitelist as $wlstring)
				{
					$wlstring = preg_quote($wlstring);
					$pattern = "~".str_replace('\*', ".*", $wlstring)."~U"; //ungreedy
					$this->_whitelist[] = $pattern;
				}
			}
		}
		$this->add_hook('authenticate', array($this, 'process'));
		$this->add_hook('login_failed', array($this, 'loginFailed'));
		$this->add_hook('login_after', array($this, 'writeCache'));
		
		if (rcmail::get_instance()->config->get('use_ceckSmtp')){
		    $this->add_hook('smtp_connect', array($this, 'configSmtp'));
		}
		
		$this->include_script('discovery.js'); // Hides the server-select-dropdown from the login-mask
	}
	
	/**
	 *	Main function 
	 *
	 */
	function process($args)
	{
		if (!isset($args['user']) || !isset($args['pass'])) return null;
		
		$isvalid = true;	
		$this->server['user'] = trim($args['user']);
		$this->server['pass'] = trim($args['pass']);
		
		if ($this->ceckCache())
		{
		    return array('host' => $this->server['imap']['host']);
		}
		
		if(!$this->getServer('imap')) return rcmail::get_instance()->config->get('default_host');
		
		if(rcmail::get_instance()->config->get('use_whitelist'))
		{
		    $isvalid = $this->isValidMailServer($this->server['imap']['host']);
		}
  
		if ($isvalid)
		{
		    if (rcmail::get_instance()->config->get('use_ceckSmtp')){
			  $this->getServer('smtp');
		    }
		    return array('host' => $this->server['imap']['host']);
		}
		else
		{
		    unset ($this->server);
		    return rcmail::get_instance()->config->get('default_host');
		}
	}
	
	/**
	 * Delete user's cache if login fails
	 */
	function loginFailed($args)
	{
	    $db = rcmail::get_instance()->db;
	    $db->query("DELETE FROM discovery_cache WHERE username = ?",$args['user']);
	    unset ($this->server);
	}
	
	
	/**
	 * Retrieves user's cache , found data are stored in a private method
	 *
	 * @return bool 
	 */
	function ceckCache()
	{
	    $db = rcmail::get_instance()->db;
	    $result = $db->query('SELECT * FROM discovery_cache WHERE username = ?',$this->server['user']);
	    if ($db->affected_rows($result) !== 0 )
	    {
		$row = $db->fetch_assoc($result);
		$this->server['imap']['host'] = $row['imap_host'];
		$this->server['smtp']['host'] = $row['smtp_host'];
		$this->server['pass'] = rcmail::get_instance()->decrypt($row['pass']);
		$this->cached = true;
		return true;
	    }
	    $this->cached = false;
	    return false;
	}
	
	
	/**
	 * Save or update the parameters found in the database 
	 *
	 */
	function writeCache($args)
	{
	    if (!isset($this->server) || $this->cached) return null;
	    
	    // Workaround for mysterious unset of $this->server['user']
	    $this->server['user'] = rcmail::get_instance()->user->get_username();
	    $host = $this->server['domain'];
	    $pass = rcmail::get_instance()->encrypt($this->server['pass']);
	    $user = $this->server['user'];
	    
	    $db = rcmail::get_instance()->db;
	    $sql = "INSERT INTO discovery_cache (username,domain,imap_host,smtp_host,pass,date) VALUES (?,?,?,?,?,NOW())";
	    $db->query($sql,array($user,$host,$this->server['imap']['host'],$this->server['smtp']['host'],$pass));
	    unset($host);
	    
	}
	
	/**
	 * Config smtp parameters
	 *
	 * @return array 
	 */
	function configSmtp($args)
	{
	   $this->server['user'] = rcmail::get_instance()->user->get_username();
	   if($this->ceckCache())
	   {
	      $result['smtp_server'] = $this->server['smtp']['host'];
	      $result['smtp_user'] = $this->server['user'];
	      $result['smtp_pass'] = $this->server['pass'];
	      return $result;
	   }
	}
	
	/**
	 * Get mail server host from email 
	 * example@gmail.com => returns mxrecord of mailserver: imap.google.com
	 * false on any error
	 *
	 * @param $email string: email address
	 * @return bool
	 */
	function getServer($protocol)
	{
		$recordA = rcmail::get_instance()->config->get('search_a');
		
		$email_parts = explode("@",$this->server['user'],2);

		if (count($email_parts) !== 2 || empty($email_parts[1])) return false;
			
		foreach($recordA[$protocol] as &$a) {
		    if (checkdnsrr($a.$email_parts[1], 'A'))
		    {
			file_put_contents("/tmp/log.txt",print_r($a.$email_parts[1],true)." $protocol \n",FILE_APPEND);
			if ($this->ceckPort($a.$email_parts[1],$protocol))
			{
			    $this->server[$protocol]['host'] = $this->server[$protocol]['crypt'].$a.$email_parts[1].':'.$this->server[$protocol]['port'];
			    $this->server['domain'] = $email_parts[1];
			    if ($protocol == 'smtp')
			    {
				if($this->ceckSmtp()) {
				    return true;
				}
				else {
				    unset($this->server[$protocol]);
				    return false;
				}
			    }
			    return true;
			}
		    }
		}
		return false;
	}

	/**
	 * Checks if the mailserver we found out via getServerByEmail 
	 * is contained in the whitelist
	 * 
	 * @param $mailserver string: Mailserver like imap.google.com
	 * @return bool
	 */
	function isValidMailServer($mailserver)
	{
		$mailserver = trim($mailserver);
		
		if (!is_string($mailserver) || empty($mailserver))
			return false;
		
		$mailserver_parts = explode(".",$mailserver,2);
		
		if (count($mailserver_parts) !== 2 || empty($mailserver_parts[1]))
			return false;

		$valid = false;

		foreach ($this->_whitelist as $serverpattern)
		{
			if (preg_match($serverpattern, $mailserver))
			{
				$valid = true;
				break;
			}
		}

		return $valid;	
	}
	
	
	/**
	 * Checks which port is open on host
	 * 
	 * @param $host string: Mailserver like imap.google.com
	 * @param $protocol string: Protocol like imap or smtp
	 * @return bool/int
	 */
	 
	function ceckPort($host,$protocol)
	{
	    $port = rcmail::get_instance()->config->get('port');
	    $crypt = rcmail::get_instance()->config->get('crypt');
	    
	    if (empty($host)) return false;
	    
	    foreach($port[$protocol] as &$p) {
		foreach($crypt as &$c) {
		    if(@$conn = fsockopen($c.$host,$p,$errno, $errstr,2))
		    {
			fclose($conn);
			$this->server[$protocol]['port'] = $p;
			$this->server[$protocol]['crypt'] = $c;
			return true;
		    }
		}
	    }
	    return false;
	}
	
	/**
	 * Checks if smtp configuration is correct
	 * 
	 * @return bool
	 */
	
	function ceckSmtp()
	{
	    rcube::get_instance()->smtp_init();
	    if(rcube::get_instance()->smtp->connect($this->server['smtp']['host'],$this->server['smtp']['port'],$this->server['user'],$this->server['pass']))
	    {
		return true;
	    }
	    else
	    {
		return false;
	    }
	}
	
}
